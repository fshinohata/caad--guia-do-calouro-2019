const links = {
	semanaDosCalouros: "http://pet.inf.ufpr.br/calouros/index.php",
	cardapioPolitecnico: "http://www.pra.ufpr.br/portal/ru/ru-centro-politecnico/",
	calendarioCEPE: "http://caad.inf.ufpr.br/guia/files/CEPE4915.pdf",
	home: `/`,
	primeirosPassos: `/primeiros-passos`,
	sobre: `/sobre`,
};

export default links;
